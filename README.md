PROJECT SPORTSEE

User page in React, fetching data from a micro API.

Project dependencies :
+ React v17.0.1
+ recharts v2.0.9
+ react-router-dom v5.2.0
+ styled-components v5.2.1
+ prop-types v15.7.2
+ Visual Studio Code (recommanded)

What you need to install and run the project :

+ Git to clone the repository
+ Yarn
+ Clone the project to your computer
git clone https://gitlab.com/saubletg/gildassaublet_12_21062021.git
+ Go to the project folder
cd GildasSaublet_12_21062021
+ Install the packages
yarn
+ Run the project (port 3001 by default)
yarn start
+ To get the backend API, fork this repo and follow the instructions
https://github.com/OpenClassrooms-Student-Center/P9-front-end-dashboard

Backend API
+ The 2 URL available (for the 2 users) are:
    + http://localhost:3001/12
    + http://localhost:3001/18
