export default class KeyData {
  constructor(calorieCount, proteinCount, carbohydrateCount, lipidCount) {
    this.calorieCount = calorieCount;
    this.proteinCount = proteinCount;
    this.carbohydrateCount = carbohydrateCount;
    this.lipidCount = lipidCount;
  
  }
}