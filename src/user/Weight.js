export default class Weight {
    constructor(day, kilogram, calories) {
      this.day = day;
      this.kilogram = kilogram;
      this.calories = calories;
    }
}