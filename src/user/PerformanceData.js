export default class PerformanceData {
    constructor(value, kind) {
      this.value = value;
      this.kind = kind;
    }
}