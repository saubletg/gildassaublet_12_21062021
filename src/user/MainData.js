export default class MainData {
  constructor(id, todayScore, userInfos, keyData) {
    this.id = id;
    this.todayScore = todayScore;
    this.userInfos = userInfos;
    this.keyData = keyData;
  }
}