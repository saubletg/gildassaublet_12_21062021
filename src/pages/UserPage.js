import React, { Component } from "react";
import styled from "styled-components";

import TopNav from "../components/TopNav";
import LeftNav from "../components/LeftNav";
import Dashboard from "../components/Dashboard";

const USERPAGE = styled.div`
  position: relative;
`;

const MAIN = styled.main`
  display: flex;
  align-items: stretch;
`;

class UserPage extends Component {

  render() {

    return (
      <USERPAGE>
        <TopNav />
        <MAIN>
          <LeftNav />
          <Dashboard />
        </MAIN>
      </USERPAGE>
      );
    }
  }
  
  export default UserPage;