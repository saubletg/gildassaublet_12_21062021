import React, { Component } from "react";
import styled from "styled-components";

const CONTAINER = styled.div`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  text-align: center;
  h1 {
    color: #e60000;
    font-weight: 700;
    font-size: 5rem;
  }
  span {
    color: #282d30;
    font-weight: 500;
    font-size: 3rem;
  }
`;

class ErrorPage extends Component {
  render() {
    return (
      <CONTAINER>
        <h1>404</h1>
        <span>Page non trouvée !</span>
      </CONTAINER>
    );
  }
}

export default ErrorPage;