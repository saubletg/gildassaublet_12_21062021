import React, { Component } from "react";
import { Route, Switch } from "react-router-dom";

import UserPage from "./UserPage";
import ErrorPage from "./ErrorPage";

class App extends Component {
  render() {
    return (
      <div className="app">
        <Switch>
          <Route exact path="/:id" component={UserPage} />
          <Route path="*" component={ErrorPage} />
        </Switch>
      </div>
    );
  }
}

export default App;
