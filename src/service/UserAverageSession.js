import AverageSession from "../user/AverageSession";
import Daily from "../user/Daily";

export default class UserAverageSession {
  /**
   * Service to handle the fetched data (user average session)
   * @param   {object}  fetchedData   The data from the API
   * @return  {object}                The formatted data for the front
   */
  getUserAverageSession(fetchedData) {
    let sessionsList = [];
    const { userId, sessions } = fetchedData;

    sessions.forEach((session) =>
      sessionsList.push(new Daily(session.day, session.sessionLength))
    );

    const averageSession = new AverageSession(userId, sessionsList);
    return averageSession;
  }
}