import UserMainData from "./UserMainData";
import UserActivity from "./UserActivity";
import UserAverageSession from "./UserAverageSession";
import UserPerformance from "./UserPerformance";

export default class Service {

  /**
   * Fetch the user main data from the API
   * @param   {number}    id        the user ID
   * @param   {function}  resolve   the function for success
   * @param   {function}  reject    the fonction to handle error
   * @return  {void}
  */

  getUserMainData = (id) => {
    return new Promise((resolve, reject) => {
      fetch(`http://localhost:3000/user/${id}`)
      .then((response) => response.json())
      .then((result) => {
        const userMainData = new UserMainData();
        resolve(userMainData.getUserMainData(result.data));
      })
      .catch((error) => {
        reject(error);
      })
    }) 
  };

  /**
   * Fetch the user main data from the API
   * @param   {number}    id        the user ID
   * @param   {function}  resolve   the function for success
   * @param   {function}  reject    the fonction to handle error
   * @return  {void}
  */

  getUserActivity = (id) => {
    return new Promise((resolve, reject) => {
      fetch(`http://localhost:3000/user/${id}/activity`)
      .then((response) => response.json())
      .then((result) => {
        const userActivity = new UserActivity();
        resolve(userActivity.getUserActivity(result.data));
      })
      .catch((error) => {
        reject(error);
      })
    }) 
  };

  /**
   * Fetch the user main data from the API
   * @param   {number}    id        the user ID
   * @param   {function}  resolve   the function for success
   * @param   {function}  reject    the fonction to handle error
   * @return  {void}
  */

  getUserAverageSession = (id) => {
    return new Promise((resolve, reject) => {
      fetch(`http://localhost:3000/user/${id}/average-sessions`)
      .then((response) => response.json())
      .then((result) => {
        const averageSession = new UserAverageSession();
        resolve(averageSession.getUserAverageSession(result.data));
      })
      .catch((error) => {
        reject(error);
      })
    }) 
  };

  /**
   * Fetch the user main data from the API
   * @param   {number}    id        the user ID
   * @param   {function}  resolve   the function for success
   * @param   {function}  reject    the fonction to handle error
   * @return  {void}
  */

  getUserPerformance = (id) => {
    return new Promise((resolve, reject) => {
      fetch(`http://localhost:3000/user/${id}/performance`)
      .then((response) => response.json())
      .then((result) => {
        const userPerformance = new UserPerformance();
        resolve(userPerformance.getUserPerformance(result.data));
      })
      .catch((error) => {
        reject(error);
      })
    }) 
  };
}