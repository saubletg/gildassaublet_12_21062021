import Performance from "../user/Performance";
import PerformanceData from "../user/PerformanceData";

export default class UserPerformance {
  /**
   * Service to handle the fetched data (user performance)
   * @param   {object}  fetchedData   The data from the API
   * @return  {object}                The formatted data for the front
   */
  getUserPerformance(fetchedData) {
    let dataList = [];
    const { userId, kind, data } = fetchedData;

    data.forEach((elt) =>
      dataList.push(new PerformanceData(elt.value, elt.kind))
    );

    const performance = new Performance(userId, kind, data);

    return performance;
  }
}