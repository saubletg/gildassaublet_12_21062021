import Weight from "../user/Weight";
import Activity from "../user/Activity";

export default class UserActivity {
  /**
   * Service to handle the fetched data (user activity)
   * @param   {object}  fetchedData   The data from the API
   * @return  {object}                The formatted data for the front
   */
  getUserActivity(fetchedData) {
    let sessionsList = [];
    const { userId, sessions } = fetchedData;

    sessions.forEach((session) =>
      sessionsList.push(
        new Weight (session.day, session.kilogram, session.calories)
      )
    );

    const activity = new Activity(userId, sessionsList);

    return activity;
  }
}