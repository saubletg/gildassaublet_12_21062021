import KeyData from "../user/KeyData";
import Infos from "../user/Infos";
import MainData from "../user/MainData";

export default class UserMainData {
  /**
   * Service to handle the fetched data (user main data)
   * @param   {object}  fetchedData   The data from the API
   * @return  {object}                The formatted data for the front
   */
  getUserMainData(fetchedData) {
    const { id, userInfos, todayScore, score, keyData } = fetchedData;
    const { firstName, lastName, age } = userInfos;
    const {
      calorieCount,
      proteinCount,
      carbohydrateCount,
      lipidCount,
    } = keyData;

    const userObj = new Infos(firstName, lastName, age);
    const keyDataObj = new KeyData(
      calorieCount,
      proteinCount,
      carbohydrateCount,
      lipidCount
    );
    const userDataObj = new MainData(id, todayScore || score, userObj, keyDataObj);

    return userDataObj;
  }
}