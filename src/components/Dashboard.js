import React, { Component } from "react";
import styled from "styled-components";

import Service from "../service/Service";

import DataTag from "./DataTag";
import ChartActivity from "../charts/Activity";
import ChartKPI from "../charts/KPI";
import ChartGoals from "../charts/Goals";
import ChartRadar from "../charts/Radar";

const CONTAINER = styled.div`
  padding: 60px 0;
  max-width: 1320px;
  width: 100%;
  margin: auto;
`;

const HEADER = styled.header`
  margin-bottom: 75px;
  padding: 0 100px;
  h1 {
    font-weight: 600;
    margin-bottom: 40px;
  }
`;

const NAME = styled.span`
  color: #ff0000;
`;

const CONTENT = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  align-items: center;
  width: 100%;
  section {
    margin: auto;
  }
  aside {
    display: flex;
    flex-wrap: wrap;
    justify-content: space-between;
    max-width: 835px;
    @media screen and (min-width: 1350px) {
      flex-direction: column;
      margin-right: 60px;
    }
    > div {
      margin: 20px 5px;
    }
  }
`;

const GROUP = styled.div`
  display: flex;
  justify-content: space-between;
`;


class Dashboard extends Component {
  constructor(props) {
    super(props);
    const url = window.location.href.split("/");
    this.state = {
      idParam: url[url.length -1],
      id:"",
      firstName: "",
      keyData: {},
    };
    console.log(this.state)
    this.service = new Service();
    
    this.updateUserMainData = this.updateUserMainData.bind(this);
  }

  componentDidMount() {
    this.service.getUserMainData(
      this.state.idParam
    ).then(data=>{
      this.updateUserMainData(data)
      console.log(this.state)
    }).catch(error=>{
      console.log(error)
    })
  }

  /**
   * Update the state with the fetched data
   * @param   {object} data the fetched data from API
   * @return  {void}
   */
  updateUserMainData(data) {
    this.setState({
      id: data.id,
      firstName: data.userInfos.firstName,
      todayScore: data.todayScore,
      keyData: data.keyData,
    });
  }

  render() {
    const { firstName, keyData, idParam } = this.state;
    const {
      calorieCount,
      proteinCount,
      carbohydrateCount,
      lipidCount,
    } = keyData;

    return (
      <CONTAINER>
        <HEADER>
          <h1>
            Bonjour <NAME>{firstName}</NAME>
          </h1>
          <span>Félicitation ! Vous avez explosé vos objectifs hier 👏</span>
        </HEADER>
        <CONTENT>
          <section>
            <ChartActivity
              idParam={idParam}
            />
            <GROUP>
              <ChartGoals
                idParam={idParam}
              />
              <ChartRadar
                idParam={idParam}
              />
              <ChartKPI
                idParam={idParam}
              />
            </GROUP>
          </section>
          <aside>
            <DataTag
              src="/./images/energy.png"
              title="Energy"
              data= {`${calorieCount}kCal`}
              type="Calories"
              color="#fbeaea"
            />
            <DataTag
              src="/./images/chicken.png"
              title="Protéines"
              data={`${proteinCount}g`}
              type="Protéines"
              color="#e9f4fb"
            />
            <DataTag
              src="/./images/apple.png"
              title="Glucides"
              data={`${carbohydrateCount}g`}
              type="Glucides"
              color="#fbf6e5"
            />
            <DataTag
              src="/./images/cheeseburger.png"
              title="Lipides"
              data={`${lipidCount}g`}
              type="Lipides"
              color="#fbeaef"
            />
          </aside>
        </CONTENT>
      </CONTAINER>
    );
  }
}

export default Dashboard;