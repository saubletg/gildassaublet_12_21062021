import React, { Component } from "react";
import styled, { keyframes } from "styled-components";
import { pulse } from 'react-animations';

import NavIcon from "./NavIcon";

const NAV = styled.nav`
  width: 120px;
  background: #000;
  padding: 60px 30px;
  position: relative;
`;

const GROUP = styled.div`
  width: 64px;
  height: 316px;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  position: absolute;
  left: 50%;
  top: calc(50% - 90px);
  transform: translate(-50%, -50%);
`;

const COPYRIGHT = styled.span`
  position: absolute;
  bottom: 60px;
  left: 50%;
  transform: translateY(-50%);
  color: #fff;
  writing-mode: tb-rl;
  transform: rotate(-180deg);
`;

const pulseAnimation = keyframes`${pulse}`;
 
const PulseDiv = styled.div`
  :hover {
      animation: 2s infinite ${pulseAnimation};
  }
`;

class LeftNav extends Component {
  render() {
    return (
      <NAV>
        <GROUP>
          <PulseDiv>
            <NavIcon path="/user" title="User Link" src="/./images/yoga.png" />
          </PulseDiv>
          <PulseDiv>
            <NavIcon path="/user" title="User Link" src="/./images/swim.png" />
          </PulseDiv>
          <PulseDiv>
            <NavIcon path="/user" title="User Link" src="/./images/cycle.png" />
          </PulseDiv>
          <PulseDiv>
            <NavIcon path="/user" title="User Link" src="/./images/iron.png" />
          </PulseDiv>
        </GROUP>
        <COPYRIGHT>Copiryght, SportSee 2020</COPYRIGHT>
      </NAV>
    );
  }
}

export default LeftNav;