import React, { Component } from "react";
import styled from "styled-components";
import { PieChart, Pie, Label, Cell, ResponsiveContainer } from "recharts";
import KPILabel from "./KPILabel";

import Service from "../service/Service";


const CONTAINER = styled.div`
  width: 258px;
  height: 263px;
  border-radius: 5px;
  background: #fbfbfb;
  .arc {
    fill: #ff0000;
  }
  .score-value {
    font-size: 1.6rem;
    font-weight: 700;
    fill: #282d30;
  }
  .score-text {
    fill: #74798c;
    font-weight: 500;
    font-size: 1.2rem;
  }
  .recharts-layer.recharts-pie-sector path {
    stroke-linecap: round;
  }
`;

const HEADER = styled.header`
  h2 {
    font-size: 1.1rem;
    font-weight: 500;
    margin: 24px 0 0 30px;
    color: #20253a;
  }
`;

class ChartKPI extends Component {
  constructor(props) {
    super(props);
    const url = window.location.href.split("/");
    this.state = {
      idParam: url[url.length -1],
      id:"",
      todayScore: null,
      data: [],
    };
    this.service = new Service();

    this.updateUserMainData = this.updateUserMainData.bind(this);
  }

  componentDidMount() {
    this.service.getUserMainData(
      this.state.idParam
    ).then(data=>{
      this.updateUserMainData(data)
      console.log(this.state)
    }).catch(error=>{
      console.log(error)
    })
  }

  /**
   * Update the state with the fetched data
   * @param   {object} data the fetched data from API
   * @return  {void}
   */
  updateUserMainData(data) {
    this.setState({
      id: data.id,
      todayScore: data.todayScore ,
      data: [
        { value: data.todayScore  },
        { value: 1 - data.todayScore  },
      ],
    });
  }
  render() {
    const {data} = this.state;

    return (
      <CONTAINER>
        <HEADER>
          <h2>Score</h2>
        </HEADER>
        <ResponsiveContainer width="100%" height="80%">
          <PieChart width={250} height={180}>
            <Pie
              data={data}
              cx="50%"
              cy="50%"
              dataKey="value"
              innerRadius={70}
              outerRadius={80}
            >
              {data.map((index) => {
                console.log(data)
                if (index === data[1]) {
                  return (
                    <Cell
                      key={`cell-${index}`}
                      fill="#fbfbfb"
                      stroke-linecap="round"
                    />
                  );
                } else {
                return <Cell key={`cell-${index}`} fill="#ff0000" />};
              })}
              <Label
                content={<KPILabel value={data[0] && data[0].value} />}
                position="center"
              />
            </Pie>
          </PieChart>
        </ResponsiveContainer>
      </CONTAINER>
    );
  }
}
  
export default ChartKPI;