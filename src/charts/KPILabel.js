import React, { Component } from "react";

class KPILabel extends Component {
  render() {
    const { value, viewBox } = this.props;

    const { cx, cy } = viewBox;
    return (
      <React.Fragment>
        <text x={cx - 15} y={cy - 5}>
          <tspan
            style={{
              fontWeight: 700,
              fontSize: "26px",
              fill: "#282d30",
              fontFamily: "Roboto",
            }}
          >
            {`${value * 100}%`}
          </tspan>
        </text>
        <text x={cx - 20} y={cy + 15}>
          <tspan
            style={{
              fontWeight: 500,
              fontSize: "16px",
              fill: "#74798c",
              fontFamily: "Roboto",
            }}
          >
            de votre
          </tspan>
        </text>
        <text x={cx - 20} y={cy + 35}>
          <tspan
            style={{
              fontWeight: 500,
              fontSize: "16px",
              fill: "#74798c",
              fontFamily: "Roboto",
            }}
          >
            objectif
          </tspan>
        </text>
      </React.Fragment>
    );
  }
}

export default KPILabel;