import React, { Component } from "react";
import styled from "styled-components";
import PropTypes from "prop-types";

const CONTAINER = styled.div`
  background: #fff;
  color: #000;
  font-size: 12px;
  font-weight: 500;
  font-family: Roboto;
  text-align: center;
  padding: 10px;
`;

class GoalsLabel extends Component {
  render() {
    const { payload, active } = this.props;

    if (active && payload) {
      return <CONTAINER>{`${payload[0].value} min`}</CONTAINER>;
    }

    return null;
  }
}

GoalsLabel.propTypes = {
  payload: PropTypes.array,
  active: PropTypes.bool,
};
export default GoalsLabel;