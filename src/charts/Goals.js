import React, { Component } from "react";
import styled from "styled-components";
import {
    LineChart,
    Line,
    XAxis,
    YAxis,
    Tooltip,
    ResponsiveContainer,
} from "recharts";
import GoalsLabel from "./GoalsLabel";

import Service from "../service/Service";

const CONTAINER = styled.div`
  width: 258px;
  height: 263px;
  border-radius: 5px;
  background: #ff0000;
  position: relative;
`;

const HEADER = styled.header`
  position: absolute;
  top: 0;
  left: 0;
  h2 {
    font-size: 15px;
    font-weight: 500;
    margin: 29px 60px 0 29px;
    line-height: 24px;
    color: #fff;
    opacity: 0.5;
  }
`;

class ChartGoals extends Component {
    constructor(props) {
      super(props);
      const url = window.location.href.split("/");
      this.state = {
        idParam: url[url.length -1],
        id: null,
        averageSessions: null
      };
      this.service = new Service();

      this.updateUserAverageSessions = this.updateUserAverageSessions.bind(this);
    }

    componentDidMount() {
      this.service.getUserAverageSession(
        this.state.idParam
      ).then(data=>{
        this.updateUserAverageSessions(data)
        console.log(this.state)
      }).catch(error=>{
        console.log(error)
      })
    }

    /**
     * Update the state with the fetched data
     * @param   {object} data the fetched data from API
     * @return  {void}
     */
    updateUserAverageSessions(data) {
      this.setState({
        id: data.userId,
        averageSessions: data.sessions,
      });
    }
    
  getXAxis(data) {
    let value = "";
    switch (data.day) {
      case 1:
        value = "L";
        break;
      case 2:
        value = "M";
        break;
      case 3:
        value = "M";
        break;
      case 4:
        value = "J";
        break;
      case 5:
        value = "V";
        break;
      case 6:
        value = "S";
        break;
      case 7:
        value = "D";
        break;
      default:
        value = "";
    }
    return value;
  }
  
  render() {
    const { averageSessions } = this.state;
    let sessionArray = [];
    let minY = 0;
    let maxY = 0;

    if (averageSessions) {
      sessionArray = averageSessions.map((elt) => elt.sessionLength);
      minY = Math.min(...sessionArray) / 2;
      maxY = Math.max(...sessionArray) * 2;
    }

    return (
      <CONTAINER>
        <HEADER>
          <h2>Durée moyenne des sessions</h2>
        </HEADER>
        {averageSessions && (
        <ResponsiveContainer width="100%" height="100%">
          <LineChart
            width={250}
            height={186}
            data={averageSessions}
            margin={{
              top: 5,
              right: 10,
              left: 10,
              bottom: 5,
            }}
          >
          <XAxis
            dataKey={this.getXAxis}
            stroke="rgba(255, 255, 255, 0.5)"
            tickLine={false}
            axisLine={false}
            tick={{ fontSize: 12 }}
          />
          <YAxis hide={true} domain={[minY, maxY]} />
            <Tooltip
              content={<GoalsLabel />}
              cursor={{
                stroke: "rgba(0, 0, 0, 0.1)",
                strokeWidth: 50,
              }}
            />
            <Line
              type="monotone"
              dataKey="sessionLength"
              stroke="#fff"
              strokeWidth={2}
              dot={false}
              activeDot={{
                stroke: "rgba(255, 255, 255, 0.2)",
                strokeWidth: 10,
                r: 5,
              }}
            />
          </LineChart>
        </ResponsiveContainer>
        )}
      </CONTAINER>
    );
  }
}
  
export default ChartGoals;