import React, { Component } from "react";
import styled from "styled-components";
import {
    Radar,
    RadarChart,
    PolarGrid,
    PolarAngleAxis,
    ResponsiveContainer,
} from "recharts";

import Service from "../service/Service";

const CONTAINER = styled.div`
  width: 258px;
  height: 263px;
  border-radius: 5px;
  background: #282d30;
`;

class ChartRadar extends Component {
  constructor(props) {
    super(props);
    const url = window.location.href.split("/");
    this.state = {
      idParam: url[url.length -1],
      id: null,
      kind: null,
      data: null,
    };
    this.service = new Service();

    this.updateUserPerformance = this.updateUserPerformance.bind(this);
    this.getAxisLabels = this.getAxisLabels.bind(this);
    
  }

  componentDidMount() {
    this.service.getUserPerformance(
      this.state.idParam
    ).then(data=>{
      this.updateUserPerformance(data)
      console.log(this.state)
    }).catch(error=>{
      console.log(error)
    })
  }

  /**
   * Update the state with the fetched data
   * @param   {object} data the fetched data from API
   * @return  {void}
   */
  updateUserPerformance(data) {
    this.setState({
      id: data.userId,
      kind: data.kind,
      data: data.data,
    });
  }

  /**
   * Change the label on the axis
   * @param   {object} data the fetched data from API
   * @return  {string} The label to display
   */
  getAxisLabels(data) {
    const { kind } = this.state;
    return kind[data.kind].charAt(0).toUpperCase() + kind[data.kind].slice(1);
  }

  render() {

    const { data } = this.state;

    return (
      <CONTAINER>
          {data && (
          <ResponsiveContainer width="100%" height="100%">
            <RadarChart cx="50%" cy="50%" outerRadius="70%" data={data}>
              <PolarGrid />
              <PolarAngleAxis
                dataKey={this.getAxisLabels}
                stroke="#fff"
                tickLine={false}
                tick={{ fontSize: 10 }}
              />
              <Radar
                dataKey="value"
                stroke="#ff0101"
                fill="#ff0101"
                fillOpacity={0.7}
              />
            </RadarChart>
          </ResponsiveContainer>
        )}
      </CONTAINER>
    );
  }
}

export default ChartRadar;