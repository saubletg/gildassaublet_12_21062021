import React, { Component } from "react";
import styled from "styled-components";
import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    ResponsiveContainer,
} from "recharts";
import ActivityLabel from "./ActivityLabel"

import Service from "../service/Service";

const CONTAINER = styled.div`
  background: #fbfbfb;
  width: 835px;
  height: 320px;
  border-radius: 5px;
  padding: 24px 30px;
  margin-bottom: 28px;
  .axis {
    color: #9b9eac;
    font-weight: 500;
    font-size: 1rem;
  }
`;

const HEADER = styled.header`
  display: flex;
  justify-content: space-between;
  align-items: center;
  h2 {
    font-weight: 500;
    font-size: 1.1rem;
  }
  .units {
    display: flex;
  }
  .header-elt {
    display: flex;
    align-items: center;
    font-weight: 500;
    color: #74798c;
    margin-left: 30px;
  }
`;

const DOT = styled.div`
  background: ${(props) => props.color};
  height: 8px;
  width: 8px;
  border-radius: 50%;
  margin: 0 10px;
`;

class ChartActivity extends Component {
    constructor(props) {
      super(props);
      const url = window.location.href.split("/");
      this.state = {
        idParam: url[url.length -1],
        id: null,
        sessions: null
      };
      this.service = new Service();

      this.updateUserActivity = this.updateUserActivity.bind(this);
    }

    componentDidMount() {
      this.service.getUserActivity(
        this.state.idParam
      ).then(data=>{
        this.updateUserActivity(data)
        console.log(this.state)
      }).catch(error=>{
        console.log(error)
      })
    }

    /**
     * Update the state with the fetched data
     * @param   {object} data the fetched data from API
     * @return  {void}
     */
    updateUserActivity(data) {
      this.setState({
       id: data.userId,
       sessions: data.sessions,
      });
    }
  
    render() {
        const { sessions } = this.state;
        let kilogramsArray = [];
        let caloriesArray = [];
        let minYKilo = 0;
        let maxYKilo = 0;
        let minYCal = 0;
        let maxYCal = 0;

        if (sessions) {
          kilogramsArray = sessions.map((elt) => elt.kilogram);
          minYKilo = Math.min(...kilogramsArray) - 1;
          maxYKilo = Math.max(...kilogramsArray) + 1;
      
          caloriesArray = sessions.map((elt) => elt.calories);
          minYCal = Math.min(...caloriesArray) - 10;
          maxYCal = Math.max(...caloriesArray) + 10;
        }
  
      return (
        <CONTAINER>
          <HEADER>
            <h2>Activité quotidienne</h2>
            <div className="units">
              <div className="header-elt">
                <DOT color="#000" />
                <span>Poids (kg)</span>
              </div>
              <div className="header-elt">
                <DOT color="#e60000" />
                <span>Calories brûlées (kCal)</span>
              </div>
            </div>
          </HEADER>
          <ResponsiveContainer width="100%" height="90%">
          <BarChart
            width={770}
            height={270}
            data={this.state.sessions}
            margin={{
              top: 50,
              right: 10,
              left: 40,
              bottom: 5,
            }}
            barCategoryGap={35}
            barGap={8}
          >
            <CartesianGrid strokeDasharray="3 3" vertical={false} />
            <XAxis
              datakey="day"
              tickLine={false}
              tick={{ fontSize: 14 }}
              dy={15}
            />
            <YAxis
              yAxisId="kilo"
              orientation="right"
              interval="number"
              allowDecimals={false}
              tickLine={false}
              axisLine={false}
              tick={{ fontSize: 14 }}
              domain={[minYKilo, maxYKilo]}
            />
            <YAxis yAxisId="cal" hide={true} domain={[minYCal, maxYCal]} />
            <Tooltip
              content={<ActivityLabel />}
              cursor={{ fill: "#e0e0e0" }}
            />
            <Bar
              yAxisId="kilo"
              dataKey="kilogram"
              fill="#000"
              radius={[50, 50, 0, 0]}
            />
            <Bar
              yAxisId="cal"
              dataKey="calories"
              fill="#e60000"
              radius={[50, 50, 0, 0]}
            />
          </BarChart>
        </ResponsiveContainer>
        </CONTAINER>
      );
    }
}
  
export default ChartActivity;