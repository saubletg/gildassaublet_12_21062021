import React, { Component } from "react";
import PropTypes from "prop-types";
import styled from "styled-components";

const CONTAINER = styled.div`
  background: #e60000;
  padding: 0 10px;
`;

const VALUE = styled.span`
  display: block;
  color: #fff;
  font-size: 12px;
  font-weight: 500;
  font-family: Roboto;
  text-align: center;
  padding: 10px 0;
`;

class ActivityLabel extends Component {
  render() {
    const { payload, active } = this.props;

    if (active && payload) {
      return (
        <CONTAINER>
          <VALUE>{`${payload[0].value}kg`}</VALUE>
          <VALUE>{`${payload[1].value}kcal`}</VALUE>
        </CONTAINER>
      );
    }

    return null;
  }
}

ActivityLabel.propTypes = {
  payload: PropTypes.array,
  active: PropTypes.bool,
};

export default ActivityLabel;